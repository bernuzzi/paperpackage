#!/bin/bash
# Initialize a folder with a LaTeX draft

HERE=$(pwd)
list_of_templates="aa aastex revtex" # ../templates/

usage () {
    echo "Usage: $(basename "$0") [OPTIONS] [NAME]"
    echo " -h  help"
    echo " -d  dry-run"
    echo " -f  specify folder (with path)"
    echo " -t  specify journal template [$list_of_templates]"
}

function exists_in_list() {
    LIST=$1
    DELIMITER=$2
    VALUE=$3
    [[ "$LIST" =~ ($DELIMITER|^)$VALUE($DELIMITER|$) ]]
}

# defaults
debug=
DEST=
TEMPLATE=

# https://wiki.bash-hackers.org/howto/getopts_tutorial
options=':hdf:t:'
while getopts $options opt; do
  case "$opt" in
      h)
	  usage && exit 1
	  ;;
      d)
	  debug=echo
	  echo "-------------------------------------------"
	  echo "DRY-RUN: NOTHING WILL BE DONE!"
	  echo "-------------------------------------------"
	  ;;
      f)
	  DEST=$OPTARG
	  ;;
      t)
	  TEMPLATE=$OPTARG
	  ;;
      :)
	  echo "Option -$OPTARG requires an argument." >&2
	  usage >&2 && exit 1
	  ;;
      \?)
	  echo "Invalid option: -$OPTARG" >&2
	  usage >&2 && exit 1
	  ;;
  esac
done
shift $((OPTIND-1))

if [ ! -z "$DEST" ] ; then
    if exists_in_list "$list_of_templates" " " "$TEMPLATE"; then
	$debug mkdir -p $DEST
	$debug cp -r ../templates/$TEMPLATE/* $DEST/
	$debug cp -r ../templates/makefile/* $DEST/	
	$debug cp ../paperpackage.py  $DEST/
	$debug cp fillbib.py $DEST/
	$debug cp getbib.sh  $DEST/
    else
	echo "unknown template (use -t <TEMPLATE>) " && exit 0
    fi

    exit 1
else
    echo "destination folder not specified (use -f <DEST>) " && exit 0
fi
