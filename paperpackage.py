#!/usr/bin/env python3
# Generate paper submission tarball

import sys, os, re
import argparse
from datetime import datetime
import shutil
import errno
from glob import glob
import string, io
import tarfile
import time

def now():
    """ Return date in form YYYYMMDD """
    return datetime.today().strftime('%Y%m%d')

def basename(filename):
    """ Get file basename """
    return re.match(r"(.+)\.(\w+)", filename).group(1)

def extension(filename):
    """ Get file extension """
    return re.match(r"(.+)\.(\w+)", filename).group(2)

def pdflatex(texfile,bibtex=True):
    """ Compile .tex file with pdflatex (command str) """    
    cmd = "pdflatex "+texfile+";"
    if bibtex:
        cmd += "bibtex "+basename(texfile)+";"
    cmd += "pdflatex "+texfile+";"
    cmd += "pdflatex "+texfile+";"
    return cmd

def tarball(folder):
    """ Tarball of a folder (command str) """
    return "tar -zcvf "+folder

def make_tarball(tarname,folder):
    """ Tarball of a folder """
    with tarfile.open(tarname, "w:gz") as tar:
        tar.add(folder, arcname=os.path.basename(folder))
    return

def remove_file_list(flist):
    """ Remove a list of files """
    for f in flist:
        if os.path.isfile(f): 
            os.remove(f)
    return

def remove_tex_temp_files(folder):
    """ Remove all .tex temporary files in folder """
    files = glob(folder+'*.aux')
    files.extend(glob(folder+'*.log'))
    files.extend(glob(folder+'*.blg'))
    files.extend(glob(folder+'*.out'))
    files.extend(glob(folder+'*Notes.bib'))
    return remove_file_list(files)

def remove_pattern(pattern,text):
    """ Remove pattern from text """
    return re.sub(pattern,"",text)

def remove_comments(text):
    """ Remove comments '%% <comment>' """
    return remove_pattern(re.compile("%% .*?\n"),text)

def substitute_pattern(pattern,substr,text):
    """ Substitute text from pattern """
    return re.sub(pattern,substr,text)

def substitute_bibliography(bbltxt,text):
    """ Substitute bibliography line """
    return substitute_pattern(r"\\bibliography\{(.+)\}",r"\\input{"+bbltxt+"}",text)

def rename_figures(text):
    """ Parse_text lines and rename figures """
    pattern_fig_beg=r"\\begin\{figure"
    pattern_fig_inc=r"\\includegraphics\[(.+)\]\{(.+)\}"
    pattern_fig_end=r"\\end\{figure"
    n = 0 # fig counter
    p = 0 # panel counter
    c = list(string.ascii_lowercase)
    buf = io.StringIO(text)
    while True:
        line = buf.readline()
        if line == "": break
        if re.search(pattern_fig_beg, line):
            # begin figure
            n = n + 1 # increment 
            p = 0     # reset
            print(f"=> figure {n:02}:", end=' ')
            continue
        fig_inc = re.search(pattern_fig_inc, line)
        if fig_inc:
            # include figure
            oldfigname = fig_inc.group(2)
            newfigname = "fig{:02}{}.".format(n,c[p])+extension(oldfigname)
            text = substitute_pattern(oldfigname,newfigname,text)
            shutil.copyfile(oldfigname,folder+"/"+newfigname)
            p = p + 1
            print(" {}".format(newfigname), end=' ')
            continue
        if re.search(pattern_fig_end, line):
            # end figure
            if p == 1:
                # correct previous figname by deleting [a-z] suffix
                refigname = "{}".format(newfigname)
                newfigname = "fig{:02}.".format(n)+extension(oldfigname)
                text = substitute_pattern(refigname,newfigname,text)
                shutil.copyfile(oldfigname,folder+"/"+newfigname)
                os.remove(folder+"/"+refigname)
                print(" -> {}".format(newfigname), end=' ')
            print("")
            continue
    return text

def copy_folder_files(flist,root):
    """ Copy folders and files in flist into root/ """
    root += "/"
    for src in flist:
        try:
            shutil.copytree(src, root+src)
        except OSError as err:
            if err.errno == errno.ENOTDIR:
                shutil.copy2(src, root+src)
            else:
                print(err)
                raise
    return
        

if __name__ == "__main__" :

    
    parser = argparse.ArgumentParser(description='Prepare tarball for LaTeX paper submission.')
    parser.add_argument("-i", "--inputex", required=True,
                        help="Input TeX file", metavar="FILE")
    parser.add_argument("-e", "--extra",  nargs="+", 
                        help="Any extra file (*.cls, *.bst, etc) or folder"+\
                        " to be copied verbatim", default=[],
                        metavar="FILE")
    args = parser.parse_args()
    
    texsource = args.inputex

    tag = now()
    workdir = os.getcwd()
    folder = "./submission"+tag
    name = "paper"+tag
    
    # Compile with pdflatex
    try:
        os.system(pdflatex(texsource))
    except Exception as msg:
        print(msg)
        raise 

    # Make dir
    if os.path.exists(folder):
        shutil.rmtree(folder)
    os.mkdir(folder)

    # Copy submission .bbl to output folder
    bbl_out = shutil.copyfile(basename(texsource)+".bbl",folder+"/"+name+".bbl")

    # Copy verbatim extra material
    copy_folder_files(args.extra,folder)
    
    # Get the text
    text = open(texsource, 'r').read()

    # Remove comments
    text = remove_comments(text)

    # Substitute .bib with .bbl
    text = substitute_bibliography(name+".bbl",text)

    # Parse text line and rename figures
    text = rename_figures(text)
    
    # Dump the submission .tex in output folder
    with open(folder+"/"+name+".tex", 'w') as f:
        f.write(text)

    time.sleep(3)
        
    # Compile pdflatex in output folder
    try:
        os.chdir(folder)
        os.system(pdflatex(name+".tex",bibtex=False))
    except Exception as msg:
        print(msg)
        raise 

    # Cleanup the output folder
    try:
        remove_tex_temp_files("./")
    except Exception as msg:
        print(msg)
        raise 

    # Move the PDF to working folder
    try:
        shutil.move(name+".pdf",workdir+"/"+name+".pdf")
    except Exception as msg:
        print(msg)
        raise 

    # Return to working folder
    os.chdir(workdir)
    
    # Make the tarball
    try:
        make_tarball(folder+".tgz",folder)
    except Exception as msg:
        print(msg)
        raise 

    print("All done!")



