# Use this makefile to produce the main pdf document of the paper. 
# Either "make" or "make all" will compile the document properly.
# "make view" will try to open the .pdf with the default viewer
# "make clean" will clean up by-products.

SHELL := /bin/bash
SRC=usage.tex
FIGS= 
BIB=local.bib # references.bib

NAMES=$(SRC:.tex=)
PDF=$(SRC:.tex=.pdf)
JUNK=.aux .bbl .blg .dvi .log .nav .out .snm .toc Notes.bib .fls .fdb_latexmk

.PHONY: $(PDF) all view clean cleantmp cleanall

all: $(PDF) 

#LATEXMK = latexmk -pdf -pdflatex="pdflatex -interaction=nonstopmode"
LATEXMK = latexmk -pdf -pdflatex="pdflatex"

$(PDF): $(SRC) $(BIB)
	$(LATEXMK) $(SRC)

view: $(SRC)
	xdg-open $(PDF) &

clean:
	latexmk -CA

cleanall: 
	@for name in $(NAMES); do  \
		for ext in $(JUNK); do \
			rm -vf $$name$$ext; \
		done;                  \
	done

cleantmp: cleanall 
	rm -vf *~

submit: cleanall
	python3 paperpackage.py $(SRC)

new: cleanall all

