# LaTeX draft: How to?

## TeX file

One file is enough for a paper, avoid multiple files. 

For LaTeX comments, please use consistently a double `%` and a space afterwards, e.g.

```
text  text text
%% comment
text  text text
```

## Bibliography

Usually we have a single, large `.bib` file in a separate repo and use
that in all the projects.
To start, the following actions are required.

- tex file uses bib from [this repo](https://bitbucket.org/bernuzzi/bibtexrefs/src/master/)
  ``` $ git clone https://bernuzzi@bitbucket.org/bernuzzi/bibtexrefs.git ```

- set a sym link to the file 
  ``` $ ln -s PATH/TO/REPOS/bibtexrefs/refs.bib references.bib ```

- do not commit/push the symlink

- for in prep or other non-standard bib entries add a `local.bib` in this repo

The bash script [getbib.sh](./getbib.sh) helps cloning, pulling the bib and creating the symlinks.
This is incorporated in the [`Makefile`](Makefile) target

```
make bib_get
```

Alternatively, the python script [fillbib.py](./fillbib.py) gets the
citations from the ADS and INSPIRE databases.
This is incorporated in the [`Makefile`](Makefile) target

```
make bib_fill
```

## Figures

Please use the following simple style, and no fancy things.

```
\begin{figure}[t]
  \centering 
    \includegraphics[width=0.49\textwidth]{ XXX.pdf }
    \caption{ XXX }
 \label{fig: XXX }
\end{figure}
```

Save figures and the scripts to generate them in `../fig/`;
save them in both PDF *and* PNG format.
One python script per figure, with the same name; can achieve this by simply adding

```
# import os, sys
fname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
plt.savefig(fname+".pdf")
plt.savefig(fname+".png", dpi=400)
```

## Submission

Use the [paperpackage.py](./paperpackage.py) to produce the tarball
for the for arxiv submission.
Please see [this repo](https://bitbucket.org/bernuzzi/paperpackage/src/master/) if you 
need to know more.


