# Paperpackage 

This repo contains a `python` script `paperpackage.py` to prepare an arxiv/paper 
submission. The script is an evolution of the previous bash script [GenerateSubmitTarb.sh](./scripts/GenerateSubmitTarb.sh).

The usage of `paperpackage.py` is documented in `usage.tex`, which describes a workflow for paper writing used in the group.

Some TeX templates for various journals are included under `templates/`.

The folder `scripts/` contains various tools for the bibliography and a [script](scripts/initdraft.sh) to initialize a folder for a LaTeX draft from the templates and tools in this repo.
